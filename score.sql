--pcd_score_break function
create or replace function public.pcd_score_breaks(
	value anyelement,
	breaks anyarray,
	scores integer[],
	right_closed boolean)
  returns integer
  language 'sql'
  cost 100
  immutable
	as $$
		select score
		from (
		  select unnest(scores) as score,
		    unnest('-Infinity'::double precision ||
					breaks::double precision[]) as break
			order by break desc
		) as breaks
		where case when right_closed then value::double precision > break
			else value::double precision >= break end
		limit 1
$$;

--pcd_score_category function
create or replace function public.pcd_score_category(
	value character varying,
	attributes anyarray,
	scores integer[])
  returns integer
  language 'sql'
  cost 100
  immutable
	as $$
		select score
		from (
			select unnest(scores) as score,
			unnest(attributes) as attr
		) as attr_score
		where value = attr
$$;

--sidewalk_segment view
create or replace view pedestrian.sidewalk_segment as
with seg_pt as (
	select pt_id,
		seg_id
	from (
		select pt.id as pt_id,
			seg.id as seg_id,
			row_number() over (partition by pt.id order by
				st_distance(pt.geom, seg.geom)) as dist_order
		from pedestrian.sidewalk_point as pt
		inner join pedestrian.sidewalk_segment_geometry as seg
			on st_dwithin(pt.geom, seg.geom, 25)
				and pt.qa_status = 'Complete'
		order by pt_id
	) as ordered_seg_pt
	where dist_order = 1
) select seg.id,
	seg.owner,
	seg.municipality,
	seg.data_source,
	seg.path_type,
	coalesce(count(distinct summary.id), 0) as summary_count,
	coalesce(sum((pt.point_type = 'Driveway')::int), 0)::integer
		as driveway_count,
	coalesce(sum((pt.point_type = 'Local Issue')::int), 0)::integer
		as local_issue_count,
	max(summary.material) as material,
	max(summary.width) as width,
	max(pt.cross_slope) as max_cross_slope,
	max(summary.surface_condition) as surface_condition,
	max(summary.vertical_fault_count) as vertical_fault_count,
	max(summary.largest_vertical_fault) as largest_vertical_fault,
	max(summary.cracked_panel_count) as cracked_panel_count,
	coalesce(string_agg(distinct case when pt.obstruction = 'None' then
		null else pt.obstruction end, '; '), 'None')
		as obstruction_types,
	max(summary.grade) as grade,
	max(summary.buffer_type) as buffer_type,
	max(summary.buffer_width) as buffer_width,
	seg.region,
	max(summary.comment) as comment,
	seg.qa_status,
	seg.qa_comment,
	seg.geom
from pedestrian.sidewalk_segment_geometry as seg
left join seg_pt
	on seg_pt.seg_id = seg.id
left join pedestrian.sidewalk_point as summary
	on seg_pt.pt_id = summary.id
		and summary.point_type = 'Summary'
left join pedestrian.sidewalk_point as pt
	on seg_pt.pt_id = pt.id
group by seg.id
order by seg.id;

--crosswalk query
drop materialized view if exists pedestrian.crosswalk_score;
create materialized view pedestrian.crosswalk_score as
select *
from (
	select distinct on (scores.id)
		scores.id, 
		scores.width,
		scores.cross_slope,
		(0.5 * scores.width + 0.5 * scores.cross_slope)::integer as compliance,
		scores.region,
		img.filename as image,
		scores.geom
	from (
		select id,
			case when marking_type in
				('No Painted Markings', 'Box for Exclusive Period')
					then 100 else
				pcd_score_breaks(cw.width,
					array[0, 36, 39, 42, 45, 48],
					array[100, 0, 20, 40, 60, 80, 100], false) end as width,
			case when cw.midblock_crossing then 100
				when cw.stop_controlled_intersection then
					pcd_score_breaks(cw.cross_slope,
					array[2.0, 4.0, 6.0, 8.0, 10.0],
					array[100, 80, 60, 40, 20, 0], true)
				when not cw.stop_controlled_intersection then
					pcd_score_breaks(cw.cross_slope,
					array[5.0, 6.0, 7.0, 8.0, 9.0],
					array[100, 80, 60, 40, 20, 0], true) end as cross_slope,
			cw.region,
			cw.geom
		from pedestrian.crosswalk_point as cw
		where cw.qa_status = 'Complete'
	) as scores
	left join pedestrian.crosswalk_point_image as img
		on img.related_id = scores.id
	order by scores.id,
		img.id
) as complete
union select id,
	null as width,
	null as cross_slope,
	null as compliance,
	region as region,
	null as image,
	geom
from pedestrian.crosswalk_point as cw
where cw.qa_status <> 'Complete';

create index sidx_crosswalk_score_geom
on pedestrian.crosswalk_score using gist(geom);

--pedestrian signal query
drop materialized view if exists pedestrian.pedestrian_signal_score;
create materialized view pedestrian.pedestrian_signal_score as
select *
from (
	select distinct on (scores.id)
		scores.id,
		case when scores.has_button then scores.button_size else null end
			as button_size,
		case when scores.has_button then scores.button_height else null end
			as button_height,
		case when scores.has_button then
			scores.button_position_appearance else null end
			as button_position_appearance,
		scores.tactile_features,
		case when scores.has_button then
			(0.2 * scores.button_size +
			0.2 * scores.button_height +
			0.3 * scores.button_position_appearance +
			0.3 * scores.tactile_features)::integer
			else scores.tactile_features end as compliance,
		scores.region,
		img.filename as image,
		scores.geom
	from (
		select id,
		ps.button_count > 0 as has_button,
		pcd_score_category(
			ps.button_size,
			array['Small', 'Medium', 'Accessible'],
			array[33, 67, 100]) as button_size,
		pcd_score_breaks(ps.button_height,
			array[5, 10, 15, 49, 54, 59],
			array[0, 20, 60, 100, 60, 20, 0], false)
			as button_height,
		(ps.button_count = 1 or ps.button_spacing)::integer * 15 +
			ps.button_offset_curb::integer * 15 +
			ps.button_surface::integer * 15 +
			ps.button_contrast::integer * 25 +
			ps.locator_tone::integer * 30
			as button_position_appearance,
		ps.tactile_arrow::integer * 50 +
			ps.vibrotactile_signal::integer * 50
			as tactile_features,
		ps.region,
		ps.geom
		from pedestrian.pedestrian_signal_point as ps
		where ps.qa_status = 'Complete'
	) as scores
	left join pedestrian.pedestrian_signal_point_image as img
		on img.related_id = scores.id
	order by scores.id,
		img.id
) as complete
union select id,
	null as button_size,
	null as button_height,
	null as button_position_appearance,
	null as tactile_features,
	null as compliance,
	region as region,
	null as image,
	geom
from pedestrian.pedestrian_signal_point as ps
where ps.qa_status <> 'Complete';

create index sidx_pedestrian_signal_score_geom
on pedestrian.pedestrian_signal_score using gist(geom);

--sidewalk_segment_geometry query
drop materialized view if exists pedestrian.sidewalk_score;
create materialized view pedestrian.sidewalk_score as
select *
from (
	select
		scores.id,
		scores.max_cross_slope,
		scores.largest_vertical_fault,
		scores.obstruction_types,
		scores.width,
		(0.25 * scores.max_cross_slope +
			0.25 * scores.largest_vertical_fault +
			0.25 * scores.obstruction_types +
			0.25 * scores.width)::integer as compliance,
		scores.surface_condition,
		scores.vertical_fault_count,
		scores.cracked_panel_count,
		(0.334 * scores.surface_condition +
			0.333 * scores.vertical_fault_count +
			0.333 * scores.cracked_panel_count)::integer as condition,
		scores.region,
		scores.geom
	from (
		select seg.id,
		pcd_score_breaks(seg.max_cross_slope,
			array[2.0, 4.0, 6.0, 8.0, 10.0],
			array[100, 80, 60, 40, 20, 0], true) as max_cross_slope,
		pcd_score_category(seg.largest_vertical_fault,
			array[
				'None',
				'Compliant',
				'0.25 - 0.50 inch, no bevel',
				'> 0.50 inch'],
			array[100, 100, 50, 0]) as largest_vertical_fault,
			case when seg.obstruction_types = 'None' then 100
			when strpos(seg.obstruction_types, ';') = 0 then 50
				else 0 end as obstruction_types,
		pcd_score_breaks(seg.width,
			array[36, 39, 42, 45, 48],
			array[0, 20, 40, 60, 80, 100], false) as width,
		case when seg.surface_condition = 'None' then 100
			else pcd_score_category(seg.surface_condition,
				array['Spalled', 'Grass', 'Dirt', 'Cracked', 'Other'],
				array[20, 40 , 60, 80, 80]) end as surface_condition,
		pcd_score_breaks((seg.vertical_fault_count /
				(ST_length(seg.geom) / 5280)),
			array[50, 100, 150, 200]::double precision[],
			array[100, 80, 60, 40, 20], false) as vertical_fault_count,
		pcd_score_breaks((seg.cracked_panel_count /
				(ST_length(seg.geom) * 0.2)),
			array[0.025, 0.05, 0.075, 0.1]::double precision[],
			array[100, 80, 60, 40, 20], false) as cracked_panel_count,
		seg.region,
		seg.geom
		from pedestrian.sidewalk_segment as seg
		where seg.summary_count = 1
	) as scores
) as complete
union select id,
	null as max_cross_slope,
	null as largest_vertical_fault,
	null as obstruction_types,
	null as width,
	null as compliance,
	null as surface_condition,
	null as vertical_fault_count,
	null as cracked_panel_count,
	null as condition,
	region as region,
	geom
from pedestrian.sidewalk_segment as seg
where seg.summary_count <> 1;

create index sidx_sidewalk_score_geom
on pedestrian.sidewalk_score using gist(geom);

--curb_ramp query
drop materialized view if exists pedestrian.curb_ramp_score;
create materialized view pedestrian.curb_ramp_score as
select *
from (
	select distinct on (scores.id)
		scores.id, 
		scores.ramp_width,
		scores.ramp_cross_slope,
		scores.ramp_running_slope,
		scores.detectable_warning_type,
		scores.detectable_warning_width, 
		scores.gutter_cross_slope,
		scores.gutter_running_slope,
		scores.landing_dimensions,
		scores.landing_slope,
		scores.approach_cross_slope,
		scores.flare_slope,
		scores.largest_vertical_fault,
		scores.obstruction,
		(0.2 * scores.ramp_width +
			0.4 * scores.ramp_cross_slope +
			0.4 * scores.ramp_running_slope)::integer as ramp_geometry,
		(0.667 * scores.detectable_warning_type +
			0.333 * scores.detectable_warning_width)::integer
			as detectable_warning,
		(0.5 * scores.gutter_cross_slope +
			0.5 * scores.gutter_running_slope)::integer as gutter,
		(0.5 * scores.landing_dimensions +
			0.5 * scores.landing_slope)::integer as landing,
		(0.5 * scores.approach_cross_slope +
			0.5 * scores.flare_slope)::integer as approach_flare,
		(0.5 * scores.largest_vertical_fault +
			0.5 * scores.obstruction)::integer as hazard,
		(0.05 * scores.ramp_width +
			0.1 * scores.ramp_cross_slope +
			0.1 * scores.ramp_running_slope +
			0.1 * scores.detectable_warning_type +
			0.05 * scores.detectable_warning_width +
			0.05 * scores.gutter_cross_slope +
			0.05 * scores.gutter_running_slope +
			0.1 * scores.landing_dimensions +
			0.1 * scores.landing_slope +
			0.05 * scores.approach_cross_slope +
			0.05 * scores.flare_slope +
			0.1 * scores.largest_vertical_fault +
			0.1 * scores.obstruction)::integer as compliance,
		scores.surface_condition,
		scores.vertical_fault_count,
		scores.cracked_panel_count,
		(0.334 * scores.surface_condition +
			0.333 * scores.vertical_fault_count +
			0.333 * scores.cracked_panel_count)::integer as condition,
		scores.region,
		img.filename as image,
		scores.geom
	from (
		select id,
		-- Ramp width
		pcd_score_breaks(cr.ramp_width,
			array[36, 39, 42, 45, 48],
			array[0, 20, 40, 60, 80, 100], false)
			as ramp_width,
		-- Ramp cross slope
		pcd_score_breaks(cr.ramp_cross_slope,
			array[2.0, 4.0, 6.0, 8.0, 10.0],
			array[100, 80, 60, 40, 20, 0], true)
			as ramp_cross_slope,
		-- Ramp running slope
		case when cr.ramp_length > 180 then 100
			else pcd_score_breaks(cr.ramp_running_slope,
			array[8.3, 9.3, 10.3],
			array[100, 67, 33, 0], true)
			end as ramp_running_slope,
		-- DWS surface type
		case when cr.gutter_running_slope <= 0 and
				cr.gutter_cross_slope <= 0 then 100
			else coalesce(pcd_score_category(cr.detectable_warning_type,
				array['Truncated Domes (Yellow)',
					'Truncated Domes (Red)',
					'Truncated Domes (Other)',
					'Pavement Grooves',
					'Other',
					'None'],
				array[100, 100, 100, 50, 50, 0]), 0)
			end as detectable_warning_type,
		-- DWS width
		case when cr.gutter_running_slope <= 0
				and cr.gutter_cross_slope <= 0 then 100
			when cr.detectable_warning_type != 'None' then
			pcd_score_breaks(
				(case when cr.ramp_type = 'Parallel' and
					cr.landing_width > 0 and
					cr.landing_length > 0 then
				cr.detectable_warning_width::numeric / (cr.landing_width - 4)
				else cr.detectable_warning_width::numeric /
					(cr.ramp_width - 4) end),
				array[0.7, 0.8, 0.9, 1.0],
				array[20, 40, 60, 80, 100], false)
			else 0 end as detectable_warning_width,
		-- Gutter cross slope
		case when cr.gutter_running_slope <= 0 and
				cr.gutter_cross_slope <= 0 then 100
			else pcd_score_breaks(cr.gutter_cross_slope,
				array[2.0, 4.0, 6.0, 8.0, 10.0],
				array[100, 80, 60, 40, 20, 0], true) end as gutter_cross_slope,
		-- Gutter running slope
		case when cr.gutter_running_slope <= 0 and
				cr.gutter_cross_slope <= 0 then 100
			else pcd_score_breaks(cr.gutter_running_slope,
				array[5.0, 7.0, 9.0, 11.0, 13.0],
				array[100, 80, 60, 40, 20, 0], true) end
				as gutter_running_slope,
		-- Landing dimensions
		case when cr.ramp_running_slope <= 5 then 100
			when cr.landing_width <= 0 and cr.landing_length <= 0 then 0
			else pcd_score_breaks(least(cr.landing_width, cr.landing_length),
				array[24, 30, 36, 42, 48],
				array[0, 20, 40, 60, 80, 100], false) end as landing_dimensions,
		-- Landing slope
		case when cr.ramp_running_slope <= 5 then 100
			when cr.landing_width <= 0 or cr.landing_length <= 0 then 0
			else pcd_score_breaks(greatest(cr.landing_running_slope,
					cr.landing_cross_slope),
				array[2.0, 4.0, 6.0, 8.0, 10.0],
				array[100, 80, 60, 40, 20, 0], true) end as landing_slope,
		--approach cross slope
		case when cr.left_approach_width + cr.right_approach_width > 0 then
			pcd_score_breaks(greatest(cr.left_approach_cross_slope,
					cr.right_approach_cross_slope),
				array[2.0, 4.0, 6.0, 8.0, 10.0],
				array[100, 80, 60, 40, 20, 0], true)
			else 100 end as approach_cross_slope,
		--flare slope
		case when cr.edge_treatment = 'Flared Sides' then
			pcd_score_breaks(cr.flare_slope,
				array[10.0, 12.0, 14.0, 16.0, 18.0],
				array[100, 80, 60, 40, 20, 0], true)
			else 100 end as flare_slope,
		-- Vertical fault size
		pcd_score_category(cr.largest_vertical_fault,
			array[
				'> 0.50 inch',
				'0.25 - 0.50 inch, no bevel',
				'Compliant',
				'None'],
			array[0, 50, 100, 100]) as largest_vertical_fault,
		-- Obstructions
		case when cr.obstruction != 'None' then 0 else 100 end as obstruction,
		case when cr.surface_condition = 'None' then 100
			else pcd_score_category(cr.surface_condition,
				array['Spalled', 'Grass', 'Dirt', 'Cracked', 'Other'],
				array[20, 40 , 60, 80, 80]) end as surface_condition,
		pcd_score_breaks(cr.vertical_fault_count,
			array[0, 1, 2, 3, 4],
			array[100, 100, 80, 60, 40, 20], false) as vertical_fault_count,
		pcd_score_breaks(cr.cracked_panel_count,
			array[0, 1, 2, 3, 4],
			array[100, 100, 80, 60, 40, 20], false) as cracked_panel_count,
		cr.region,
		cr.geom
		from pedestrian.curb_ramp_point as cr
		where cr.qa_status = 'Complete'
			and cr.ramp_type != 'None'
	) as scores
	left join pedestrian.curb_ramp_point_image as img
		on img.related_id = scores.id
	order by scores.id,
		img.id
) as complete
union select distinct on (cr.id)
	cr.id,
	null as ramp_width,
	null as ramp_cross_slope,
	null as ramp_running_slope,
	null as detectable_warning_type,
	null as detectable_warning_width,
	null as gutter_cross_slope,
	null as gutter_running_slope,
	null as landing_dimensions,
	null as landing_slope,
	null as approach_cross_slope,
	null as flare_slope,
	null as largest_vertical_fault,
	null as obstruction,
	null as ramp_geometry,
	null as detectable_warning,
	null as gutter,
	null as landing,
	null as approach_flare,
	null as hazard,
	null as compliance,
	null as surface_condition,
	null as vertical_fault_count,
	null as cracked_panel_count,
	null as condition,
	cr.region as region,
	img.filename as image,
	cr.geom
from pedestrian.curb_ramp_point as cr
left join pedestrian.curb_ramp_point_image as img
	on img.related_id = cr.id
		and cr.ramp_type = 'None'
		and cr.qa_status = 'Complete'
where cr.qa_status <> 'Complete'
	or cr.ramp_type = 'None';

create index sidx_curb_ramp_score_geom
on pedestrian.curb_ramp_score using gist(geom);

-- score hexagon view
drop materialized view if exists pedestrian.sidewalk_network_score_hexagon;
create materialized view pedestrian.sidewalk_network_score_hexagon as
with sw as (
    -- aggregate sidewalk scores
    select hexagon_id,
        round(sum(max_cross_slope * weight) / sum(weight)) as max_cross_slope,
        round(sum(largest_vertical_fault * weight) / sum(weight))
			as largest_vertical_fault,
        round(sum(obstruction_types * weight) / sum(weight))
			as obstruction_types,
        round(sum(width * weight) / sum(weight)) as width,
        round(sum(compliance * weight) / sum(weight)) as compliance,
        round(sum(surface_condition * weight) / sum(weight))
			as surface_condition,
        round(sum(vertical_fault_count * weight) / sum(weight))
			as vertical_fault_count,
        round(sum(cracked_panel_count * weight) / sum(weight))
			as cracked_panel_count,
        round(sum(condition * weight) / sum(weight)) as condition,
        count(*) as feature_count
    from (
        select hexagon.id as hexagon_id,
            st_length(st_intersection(sidewalk.geom, hexagon.geom)) as weight,
            sidewalk.*
        from boundary.champaign_county_hexagon as hexagon
        inner join pedestrian.sidewalk_score as sidewalk
            on st_intersects(sidewalk.geom, hexagon.geom)
                and sidewalk.compliance is distinct from null
    ) as weights
    group by (hexagon_id)
), cr as (
    -- aggregate curb ramp scores
    select hexagon.id as hexagon_id,
        round(avg(curb_ramp.ramp_width)) as ramp_width,
        round(avg(curb_ramp.ramp_cross_slope)) as ramp_cross_slope,
        round(avg(curb_ramp.ramp_running_slope)) as ramp_running_slope,
        round(avg(curb_ramp.detectable_warning_type))
			as detectable_warning_type,
        round(avg(curb_ramp.detectable_warning_width))
			as detectable_warning_width,
        round(avg(curb_ramp.gutter_cross_slope)) as gutter_cross_slope,
        round(avg(curb_ramp.gutter_running_slope)) as gutter_running_slope,
        round(avg(curb_ramp.landing_dimensions)) as landing_dimensions,
        round(avg(curb_ramp.landing_slope)) as landing_slope,
        round(avg(curb_ramp.approach_cross_slope)) as approach_cross_slope,
        round(avg(curb_ramp.flare_slope)) as flare_slope,
        round(avg(curb_ramp.largest_vertical_fault)) as largest_vertical_fault,
        round(avg(curb_ramp.obstruction)) as obstruction,
        round(avg(curb_ramp.ramp_geometry)) as ramp_geometry,
        round(avg(curb_ramp.detectable_warning)) as detectable_warning,
        round(avg(curb_ramp.gutter)) as gutter,
        round(avg(curb_ramp.landing)) as landing,
        round(avg(curb_ramp.approach_flare)) as approach_flare,
        round(avg(curb_ramp.hazard)) as hazard,
        round(avg(curb_ramp.compliance)) as compliance,
        round(avg(curb_ramp.surface_condition)) as surface_condition,
        round(avg(curb_ramp.vertical_fault_count)) as vertical_fault_count,
        round(avg(curb_ramp.cracked_panel_count)) as cracked_panel_count,
        round(avg(curb_ramp.condition)) as condition,
        count(*) as feature_count
    from boundary.champaign_county_hexagon as hexagon
    inner join pedestrian.curb_ramp_score as curb_ramp
        on st_intersects(curb_ramp.geom, hexagon.geom)
            and curb_ramp.compliance is distinct from null
    group by (hexagon.id)
), cw as (
    -- aggregate crosswalk scores
    select hexagon.id as hexagon_id,
        round(avg(crosswalk.width)) as width,
        round(avg(crosswalk.cross_slope)) as cross_slope,
        round(avg(crosswalk.compliance)) as compliance,
        count(*) as feature_count
    from boundary.champaign_county_hexagon as hexagon
    inner join pedestrian.crosswalk_score as crosswalk
        on st_intersects(crosswalk.geom, hexagon.geom)
            and crosswalk.compliance is distinct from null
    group by (hexagon.id)
), ps as (
    -- aggregate pedestrian signal scores
    select hexagon.id as hexagon_id,
        round(avg(pedestrian_signal.button_size)) as button_size,
        round(avg(pedestrian_signal.button_height)) as button_height,
        round(avg(pedestrian_signal.button_position_appearance))
			as button_position_appearance,
        round(avg(pedestrian_signal.tactile_features)) as tactile_features,
        round(avg(pedestrian_signal.compliance)) as compliance,
        count(*) as feature_count
    from boundary.champaign_county_hexagon as hexagon
    inner join pedestrian.pedestrian_signal_score as pedestrian_signal
        on st_intersects(pedestrian_signal.geom, hexagon.geom)
            and pedestrian_signal.compliance is distinct from null
    group by (hexagon.id)
)
-- combine aggregated scores
select hexagon.id,
    sw.feature_count as sidewalk_count,
    sw.max_cross_slope as sidewalk_max_cross_slope,
    sw.largest_vertical_fault as sidewalk_largest_vertical_fault,
    sw.obstruction_types as sidewalk_obstruction_types,
    sw.width as sidewalk_width,
    sw.compliance as sidewalk_compliance,
    sw.surface_condition as sidewalk_surface_condition,
    sw.vertical_fault_count as sidewalk_vertical_fault_count,
    sw.cracked_panel_count as sidewalk_cracked_panel_count,
    sw.condition as sidewalk_condition,
    cr.feature_count as curb_ramp_count,
    cr.ramp_width as curb_ramp_ramp_width,
    cr.ramp_cross_slope as curb_ramp_ramp_cross_slope,
    cr.ramp_running_slope as curb_ramp_ramp_running_slope,
    cr.detectable_warning_type as curb_ramp_detectable_warning_type,
    cr.detectable_warning_width as curb_ramp_detectable_warning_width,
    cr.gutter_cross_slope as curb_ramp_gutter_cross_slope,
    cr.gutter_running_slope as curb_ramp_gutter_running_slope,
    cr.landing_dimensions as curb_ramp_landing_dimensions,
    cr.landing_slope as curb_ramp_landing_slope,
    cr.approach_cross_slope as curb_ramp_approach_cross_slope,
    cr.flare_slope as curb_ramp_flare_slope,
    cr.largest_vertical_fault as curb_ramp_largest_vertical_fault,
    cr.obstruction as curb_ramp_obstruction,
    cr.ramp_geometry as curb_ramp_ramp_geometry,
    cr.detectable_warning as curb_ramp_detectable_warning,
    cr.gutter as curb_ramp_gutter,
    cr.landing as curb_ramp_landing,
    cr.approach_flare as curb_ramp_approach_flare,
    cr.hazard as curb_ramp_hazard,
    cr.compliance as curb_ramp_compliance,
    cr.surface_condition as curb_ramp_surface_condition,
    cr.vertical_fault_count as curb_ramp_vertical_fault_count,
    cr.cracked_panel_count as curb_ramp_cracked_panel_count,
    cr.condition as curb_ramp_condition,
    cw.feature_count as crosswalk_count,
    cw.width as crosswalk_width,
    cw.cross_slope as crosswalk_cross_slope,
    cw.compliance as crosswalk_compliance,
    ps.feature_count as pedestrian_signal_count,
    ps.button_size as pedestrian_signal_button_size,
    ps.button_height as pedestrian_signal_button_height,
    ps.button_position_appearance
		as pedestrian_signal_button_position_appearance,
    ps.tactile_features as pedestrian_signal_tactile_features,
    ps.compliance as pedestrian_signal_compliance,
    hexagon.region,
    hexagon.geom
from boundary.champaign_county_hexagon as hexagon
left join sw
    on sw.hexagon_id = hexagon.id
left join cr
    on cr.hexagon_id = hexagon.id
left join cw
    on cw.hexagon_id = hexagon.id
left join ps
    on ps.hexagon_id = hexagon.id;

create index sidx_sidewalk_network_score_hexagon_geom
on pedestrian.sidewalk_network_score_hexagon using gist(geom);

-- image views
create or replace view pedestrian.crosswalk_point_image_url as
select id,
    related_id,
    'https://maps.ccrpc.org/images/pcd/pedestrian/crosswalk_point/' ||
		filename as url
from pedestrian.crosswalk_point_image;

create or replace view pedestrian.curb_ramp_point_image_url as
select id,
    related_id,
    'https://maps.ccrpc.org/images/pcd/pedestrian/curb_ramp_point/' ||
		filename as url
from pedestrian.curb_ramp_point_image;

create or replace view pedestrian.pedestrian_signal_point_image_url as
select id,
    related_id,
    'https://maps.ccrpc.org/images/pcd/pedestrian/pedestrian_signal_point/' ||
		filename as url
from pedestrian.pedestrian_signal_point_image;
