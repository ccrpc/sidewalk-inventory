create temporary table sidewalk_region_buffer as
select region,
    ST_Union(geom) as geom
from (
    select case when name in ('Rantoul', 'Mahomet') then name
        else 'Urban Area' end as region,        --verify that Urban Area is ready to be used, as opposed to Urbanized Area
        ST_Buffer(geom, 2640) as geom
    from boundary.municipal
    where name in (
        'Champaign',
        'Urbana',
        'Savoy',
        'Bondville',
        'Tolono',
        'Rantoul',
        'Mahomet'
    )
) as buffer
group by region;

create index sidx_sidewalk_region_buffer_geom
on sidewalk_region_buffer using gist(geom);

update pedestrian.crosswalk_point as pt
set region = buffer.region
from sidewalk_region_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.curb_ramp_point as pt
set region = buffer.region
from sidewalk_region_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.pedestrian_signal_point as pt
set region = buffer.region
from sidewalk_region_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.sidewalk_point as pt
set region = buffer.region
from sidewalk_region_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.sidewalk_segment_geometry as seg
set region = buffer.region
from sidewalk_region_buffer as buffer
where ST_Intersects(seg.geom, buffer.geom);
