with regions as (
    select distinct region
    from pedestrian.curb_ramp_score
),
cw as (
	select score.*,
		feature.midblock_crossing,
        feature.marking_type = 'No Painted Markings' as no_painted_markings
	from pedestrian.crosswalk_score as score
	inner join sidewalk.crosswalk as feature
		on score.id = feature.id
			and score.compliance is distinct from null
),
cr as (
	select score.*,
		feature.ramp_length > 15*12 as long_ramp,
		feature.gutter_cross_slope = 0
            or feature.gutter_running_slope = 0 as upper_ramp,
		feature.landing_width = 0 or feature.landing_length = 0 as no_landing,
		feature.right_approach_width = 0
            and feature.left_approach_width = 0 as no_approaches,
		feature.edge_treatment <> 'Flared Sides' as no_flares,
		feature.ramp_running_slope <= 5.0 as blended_transition,
        feature.detectable_warning_type = 'Other' as dws_other,
        feature.surface_condition = 'Other' as surface_condition_other
	from pedestrian.curb_ramp_score as score
	inner join sidewalk.curb_ramp as feature
		on score.id = feature.id
			and score.compliance is distinct from null
),
ps as (
	select score.*,
	    feature.button_location = 'No Button' as no_button,
        feature.button_height < 15 as button_height_low,
        feature.button_count,
        feature.button_spacing,
        feature.button_offset_curb,
        feature.button_surface,
        feature.button_contrast,
        feature.locator_tone,
        feature.tactile_arrow,
        feature.vibrotactile_signal
	from pedestrian.pedestrian_signal_score as score
	inner join pedestrian.pedestrian_signal_point as feature
		on score.id = feature.id
			and score.compliance is distinct from null
),
sw as (
    select score.*,
	    feature.surface_condition = 'Other' as surface_condition_other,
        ST_Length(feature.geom)/5280 as length_miles
	from pedestrian.sidewalk_score as score
	inner join sidewalk.sidewalk as feature
		on score.id = feature.id
			and score.compliance is distinct from null
),
fields as (
    select region,
        'crosswalk' as feature_type,
        1 as value,
        unnest(array[
            'width',
            'cross_slope',
            'compliance'
        ]) as field,
        unnest(array[
            case when no_painted_markings then -10 else width end,
            case when midblock_crossing then -10 else cross_slope end,
            pcd_score_breaks(compliance,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true)
        ]) as weight
    from cw
    union all select region,
        'curb_ramp' as feature_type,
        1 as value,
        unnest(array[
            'ramp_width',
            'ramp_cross_slope',
            'ramp_running_slope',
            'detectable_warning_type',
            'detectable_warning_width',
            'gutter_cross_slope',
            'gutter_running_slope',
            'landing_dimensions',
            'landing_slope',
            'approach_cross_slope',
            'flare_slope',
            'largest_vertical_fault',
            'obstruction',
            'compliance',
            'surface_condition',
            'vertical_fault_count',
            'cracked_panel_count',
            'condition'
        ]) as field,
        unnest(array[
            ramp_width,
            ramp_cross_slope,
            case when long_ramp then -10 else ramp_running_slope end,
            case when upper_ramp then -10 when dws_other then 25
                else detectable_warning_type end,
            case when upper_ramp then -10 else detectable_warning_width end,
            case when upper_ramp then -10 else gutter_cross_slope end,
            case when upper_ramp then -10 else gutter_running_slope end,
            case when blended_transition then -20 when no_landing then -10
                else landing_dimensions end,
            case when blended_transition then -20 when no_landing then -10
                else landing_slope end,
            case when no_approaches then -10 else approach_cross_slope end,
            case when no_flares then -10 else flare_slope end,
            largest_vertical_fault,
            obstruction,
            pcd_score_breaks(compliance,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true),
            case when surface_condition_other then 90
                else surface_condition end,
            vertical_fault_count,
            cracked_panel_count,
            pcd_score_breaks(condition,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true)
        ]) as weight
    from cr
    union all select region,
        'pedestrian_signal' as feature_type,
        1 as value,
        unnest(array[
            'button_size',
            'button_height',
            'button_position_appearance',
            'button_position_appearance',
            'button_position_appearance',
            'button_position_appearance',
            'button_position_appearance',
            'tactile_features',
            'tactile_features',
            'compliance'
        ]) as field,
        unnest(array[
            case when no_button then -10 else button_size end,
            case when no_button then -10
                when button_height_low then 200 - button_height
                else button_height end,
            case when button_spacing then 50 else 0 end,
            case when button_offset_curb then 40 else 0 end,
            case when button_surface then 30 else 0 end,
            case when button_contrast then 20 else 0 end,
            case when locator_tone then 10 else 0 end,
            case when tactile_arrow then 20 else 0 end,
            case when vibrotactile_signal then 10 else 0 end,
            pcd_score_breaks(compliance,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true)
        ]) as weight
    from ps
    union all select region,
        'sidewalk' as feature_type,
        length_miles as value,
        unnest(array[
            'max_cross_slope',
            'largest_vertical_fault',
            'obstruction_types',
            'width',
            'compliance',
            'surface_condition',
            'vertical_fault_count',
            'cracked_panel_count',
            'condition'
        ]) as field,
        unnest(array[
            max_cross_slope,
            largest_vertical_fault,
            obstruction_types,
            width,
            pcd_score_breaks(compliance,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true),
            case when surface_condition_other then 90
                else surface_condition end,
            vertical_fault_count,
            cracked_panel_count,
            pcd_score_breaks(condition,
                array[60, 70, 80, 90], array[1, 2, 3, 4, 5], true)
        ]) as weight
    from sw
),
levels as (
    select 'crosswalk' as feature_type,
        field,
        unnest(weights) as weight
    from (values 
        ('width', array[100, 80, 60, 40, 20, 0, -10]),
        ('cross_slope', array[100, 80, 60, 40, 20, 0, -10]),
        ('compliance', array[5, 4, 3, 2, 1])
    ) as cw_weights (field, weights)
    union select 'curb_ramp' as feature_type,
        field,
        unnest(weights) as weight
    from (values 
        ('ramp_width', array[100, 80, 60, 40, 20, 0]),
        ('ramp_cross_slope', array[100, 80, 60, 40, 20, 0]),
        ('ramp_running_slope', array[100, 67, 33, 0, -10]),
        ('detectable_warning_type', array[100, 50, 25, 0, -10]),
        ('detectable_warning_width', array[100, 80, 60, 40, 20, 0, -10]),
        ('gutter_cross_slope', array[100, 80, 60, 40, 20, 0, -10]),
        ('gutter_running_slope', array[100, 80, 60, 40, 20, 0, -10]),
        ('landing_dimensions', array[100, 80, 60, 40, 20, 0, -10, -20]),
        ('landing_slope', array[100, 80, 60, 40, 20, 0, -10, -20]),
        ('approach_cross_slope', array[100, 80, 60, 40, 20, 0, -10]),
        ('flare_slope', array[100, 80, 60, 40, 20, 0, -10]),
        ('largest_vertical_fault', array[100, 50, 0]),
        ('obstruction', array[100, 0]),
        ('compliance', array[5, 4, 3, 2, 1]),
        ('surface_condition', array[100, 90, 80, 60, 40, 20]),
        ('vertical_fault_count', array[100, 80, 60, 40, 20]),
        ('cracked_panel_count', array[100, 80, 60, 40, 20]),
        ('condition', array[5, 4, 3, 2, 1])
    ) as cr_weights (field, weights)
    union select 'pedestrian_signal' as feature_type,
        field,
        unnest(weights) as weight
    from (values 
        ('button_size', array[100, 67, 33, -10]),
        ('button_height', array[200, 180, 140, 100, 60, 20, 0, -10]),
        ('button_position_appearance', array[50, 40, 30, 20, 10]),
        ('tactile_features', array[20, 10]),
        ('compliance', array[5, 4, 3, 2, 1])
    ) as ps_weights (field, weights)
    union select 'sidewalk' as feature_type,
        field,
        unnest(weights) as weight
    from (values 
        ('max_cross_slope', array[100, 80, 60, 40, 20, 0]),
        ('largest_vertical_fault', array[100, 50, 0]),
        ('obstruction_types', array[100, 50, 0]),
        ('width', array[100, 80, 60, 40, 20, 0]),
        ('compliance', array[5, 4, 3, 2, 1]),
        ('surface_condition', array[100, 90, 80, 60, 40, 20]),
        ('vertical_fault_count', array[100, 80, 60, 40, 20]),
        ('cracked_panel_count', array[100, 80, 60, 40, 20]),
        ('condition', array[5, 4, 3, 2, 1])
    ) as sw_weights (field, weights)
),
totals as (
	select region,
        feature_type,
		field,
		weight,
		round(sum(value)::numeric, (feature_type = 'sidewalk')::integer)
            as value
	from fields
	group by region,
        feature_type,
		field,
		weight
),
unpacked as (
    select regions.region,
        levels.feature_type,
        levels.field,
        array_agg(coalesce(totals.value, 0)
            order by levels.weight desc) as counts
    from levels
    inner join regions
        on true
    left join totals
        on totals.region = regions.region
            and totals.feature_type = levels.feature_type
            and totals.field = levels.field
            and totals.weight = levels.weight
    group by regions.region,
        levels.feature_type,
        levels.field
)
select
    region,
	replace(
		replace(
			replace(json_object(array_agg(feature_type),
				array_agg(feature_json::text))::text, '\', ''),
			'"{', '{'),
		'}"', '}') as feature_json
from (
    select region,
        feature_type,
        replace(
			replace(
				json_object(array_agg(field),
				array_agg(to_json(counts)::text))::text, '"[', '['),
			']"', ']') as feature_json
    from unpacked
    group by region,
        feature_type
    order by region,
        feature_type
) as by_feature_type
group by region;
