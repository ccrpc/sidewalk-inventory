create temporary table sidewalk_municipality_buffer as
select municipality,
    ST_Union(geom) as geom
from (
    select name as municipality,
    ST_Buffer(geom, 1) as geom
    from boundary.municipal
) as buffer
group by municipality;

create index sidx_sidewalk_municipality_buffer_geom
on sidewalk_municipality_buffer using gist(geom);

update pedestrian.crosswalk_point as pt
set municipality = buffer.municipality
from sidewalk_municipality_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.curb_ramp_point as pt
set municipality = buffer.municipality
from sidewalk_municipality_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.pedestrian_signal_point as pt
set municipality = buffer.municipality
from sidewalk_municipality_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.sidewalk_point as pt
set municipality = buffer.municipality
from sidewalk_municipality_buffer as buffer
where ST_Intersects(pt.geom, buffer.geom);

update pedestrian.sidewalk_segment_geometry as seg
set municipality = buffer.municipality
from sidewalk_municipality_buffer as buffer
where ST_Intersects(seg.geom, buffer.geom);