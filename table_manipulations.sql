-- The following are useful SQL queries that may be utilized at the end of the sidewalk upload.
-- They have been provided as a resource and are not meant to be ran in any order. 
-- Refer to the dokuwiki instructions list for more guidance (dt:sidewalk-network-updates).

------------------------------------
-- COPYING MATERIALIZED VIEWS
------------------------------------ 
-- from pedestrian -> sidewalk schema

CREATE MATERIALIZED VIEW sidewalk.crosswalk_score_2024 AS
SELECT *
FROM pedestrian.crosswalk_score
WITH DATA;


------------------
-- COPYING TABLES
------------------
-- can be performed for converting materialized views
-- into tables as well
CREATE TABLE sidewalk.crosswalk_score_2024 AS
SELECT *
FROM pedestrian.crosswalk_score;


----------------------------------------
-- CONVERTING TO ARCMAP COMPATIBLE DATA
----------------------------------------
-- requires a change for tolerance of vertices using ST_SnapToGrid.

-- UPDATE query (used in October 2024):
-- requires the user to create the table first, with the appropriate name, 
-- and then run the update query below.
UPDATE sidewalk.arcmap_curb_ramp_2024 
SET geom = ST_RemoveRepeatedPoints(ST_SnapToGrid(geom, 0.001));

/*
-- CREATE query (not used nor attempted in oct. 2024)
-- St_SnapToGrid performed during table creation:
CREATE TABLE sidewalk.crosswalk_score_2024 AS
SELECT 
    *
    ST_RemoveRepeatedPoints(ST_SnapToGrid(geom, 0.001)) AS geom, 
FROM pedestrian.crosswalk_score;
*/

-------------------
-- MOVING TABLES 
-------------------
ALTER TABLE sidewalk.ccgisc_curb_ramp
	SET SCHEMA historical_sidewalk_data;


--------------------
-- RENAMING TABLES
--------------------
ALTER TABLE historical_sidewalk_data.ccgisc_sidewalk
	RENAME TO arcmap_sidewalk_2023;
