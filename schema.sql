-- Drop old tables
drop table if exists pedestrian.crosswalk_point_image;
drop table if exists pedestrian.curb_ramp_point_image;
drop table if exists pedestrian.pedestrian_signal_point_image;
drop table if exists pedestrian.sidewalk_point_image;

drop table if exists pedestrian.crosswalk_point;
drop table if exists pedestrian.curb_ramp_point;
drop table if exists pedestrian.pedestrian_signal_point;
drop table if exists pedestrian.sidewalk_point;
drop table if exists pedestrian.sidewalk_segment_geometry;

-- Create domains
create domain public.qa_status as character varying(25)
check (value in (
  'Not Started',
  'Needs Field Review',
  'Needs Staff Review',
  'Complete',
  'Deferred'
));

create domain pedestrian.material as character varying(25)
check (value in (
  'Aggregate',
  'Asphalt',
  'Brick',
  'Concrete',
  'Other'
));

create domain pedestrian.largest_vertical_fault as character varying(30)
check (value in (
  'Compliant',
  '0.25 - 0.50 inch, no bevel',
  'None',
  '> 0.50 inch'
));

create domain pedestrian.obstruction as character varying(30)
check (value in (
  'Pole or signpost',
  'Hydrant',
  'Bollard',
  'Grate',
  'Tree roots',
  'Tree trunk or other vegetation',
  'Other',
  'None'
));

create domain pedestrian.surface_condition as character varying(25)
check (value in (
  'Spalled',
  'Grass',
  'Dirt',
  'Cracked',
  'Other',
  'None'
));

-- Crosswalk point table
create table pedestrian.crosswalk_point (
    id serial primary key,
    marking_type character varying(25),
    material pedestrian.material,
    width integer,
    cross_slope numeric(3,1),
    stop_controlled_intersection boolean,
    midblock_crossing boolean,
    collection_date timestamp without time zone,
    region character varying(25),
    comment character varying(200),
    qa_status qa_status not null default 'Complete',
    qa_comment character varying(200),
    geom geometry(Point,3435) not null
);

alter table pedestrian.crosswalk_point
add constraint check_required_fields check (
  case when qa_status in ('Not Started', 'Complete') then
    width is distinct from null and
    cross_slope is distinct from null and
    marking_type is distinct from null and
    stop_controlled_intersection is distinct from null and
    midblock_crossing is distinct from null and
    collection_date is distinct from null
  else true end
);

alter table pedestrian.crosswalk_point
add constraint check_slopes check (
  cross_slope <= 50.0
);

alter table pedestrian.crosswalk_point
add constraint check_marking_type check (
  marking_type in (
    'Solid',
    'Standard',
    'Continental',
    'Dashed',
    'Zebra',
    'Ladder',
    'Box for Exclusive Period',
    'Other',
    'No Painted Markings'
  )
);

alter table pedestrian.crosswalk_point
add constraint check_marking_type_width check (
  (marking_type in ('Box for Exclusive Period', 'No Painted Markings')) =
  (width = 0)
);

create index sidx_crosswalk_point_geom
on pedestrian.crosswalk_point using gist(geom);

-- Crosswalk image table
create table pedestrian.crosswalk_point_image (
    id serial primary key,
    related_id integer references pedestrian.crosswalk_point(id)
        on delete cascade on update cascade,
    filename character varying(255)
);

-- Curb ramp point table
create table pedestrian.curb_ramp_point (
    id serial primary key,
    ramp_type character varying(25),
    edge_treatment character varying(25),
    material pedestrian.material,
    in_median boolean,
    ramp_width integer,
    ramp_length integer,
    ramp_running_slope numeric(3,1),
    ramp_cross_slope numeric(3,1),
    detectable_warning_type character varying(25),
    detectable_warning_width integer,
    detectable_warning_length integer,
    gutter_running_slope numeric(3,1),
    gutter_cross_slope numeric(3,1),
    landing_width integer,
    landing_length integer,
    landing_running_slope numeric(3,1),
    landing_cross_slope numeric(3,1),
    left_approach_width integer,
    left_approach_running_slope numeric(3,1),
    left_approach_cross_slope numeric(3,1),
    right_approach_width integer,
    right_approach_running_slope numeric(3,1),
    right_approach_cross_slope numeric(3,1),
    flare_slope numeric(3,1),
    vertical_fault_count integer,
    largest_vertical_fault pedestrian.largest_vertical_fault,
    cracked_panel_count integer,
    surface_condition pedestrian.surface_condition,
    obstruction pedestrian.obstruction,
    owner character varying(50),
    collection_date timestamp without time zone,
    region character varying(25),
    comment character varying(200),
    qa_status qa_status not null default 'Not Started',
    qa_comment character varying(200),
    geom geometry(Point,3435) not null
);

alter table pedestrian.curb_ramp_point
add constraint check_required_fields check (
  case when qa_status in ('Not Started', 'Complete') then
    ramp_type is distinct from null and
    collection_date is distinct from null and (
      case when not (ramp_type = 'None') then
        material is distinct from null and
        ramp_width is distinct from null and
        ramp_length is distinct from null and
        ramp_running_slope is distinct from null and
        ramp_cross_slope is distinct from null and
        detectable_warning_type is distinct from null and
        detectable_warning_width is distinct from null and
        detectable_warning_length is distinct from null and
        gutter_running_slope is distinct from null and
        gutter_cross_slope is distinct from null and
        landing_width is distinct from null and
        landing_length is distinct from null and
        landing_running_slope is distinct from null and
        landing_cross_slope is distinct from null and
        left_approach_width is distinct from null and
        left_approach_running_slope is distinct from null and
        left_approach_cross_slope is distinct from null and
        right_approach_width is distinct from null and
        right_approach_running_slope is distinct from null and
        right_approach_cross_slope is distinct from null and
        flare_slope is distinct from null and
        vertical_fault_count is distinct from null and
        largest_vertical_fault is distinct from null and
        cracked_panel_count is distinct from null and
        surface_condition is distinct from null and
        obstruction is distinct from null and
        in_median is distinct from null and
        edge_treatment is distinct from null
      else true end
    )
  else true end
);

alter table pedestrian.curb_ramp_point
add constraint check_slopes check (
  ramp_running_slope <= 50.0 and
  ramp_cross_slope <= 50.0 and
  gutter_running_slope <= 50.0 and
  gutter_cross_slope <= 50.0 and
  landing_running_slope <= 50.0 and
  landing_cross_slope <= 50.0 and
  left_approach_running_slope <= 50.0 and
  left_approach_cross_slope <= 50.0 and
  right_approach_running_slope <= 50.0 and
  right_approach_cross_slope <= 50.0 and
  flare_slope <= 50.0
);

alter table pedestrian.curb_ramp_point
add constraint check_ramp_type check (
  ramp_type in (
    'Blended Transition',
    'Built-up',
    'Combination',
    'Diagonal',
    'None',
    'Other',
    'Parallel',
    'Perpendicular'
  )
);

alter table pedestrian.curb_ramp_point
add constraint check_detectable_warning_type check (
  detectable_warning_type in (
    'Other',
    'None',
    'Truncated Domes (Yellow)',
    'Truncated Domes (Other)',
    'Truncated Domes (Red)',
    'Pavement Grooves'
  )
);

alter table pedestrian.curb_ramp_point
add constraint check_edge_treatment check (
  edge_treatment in (
    'Other',
    'Flat Edges',
    'Returned Curbs',
    'Flared Sides'
  )
);

alter table pedestrian.curb_ramp_point
add constraint check_vertical_faults check (
  (largest_vertical_fault = 'None') = (vertical_fault_count = 0)
);

alter table pedestrian.curb_ramp_point
add constraint check_flares check (
  (edge_treatment = 'Flared Sides') = (flare_slope > 0)
);

alter table pedestrian.curb_ramp_point
add constraint check_detectable_warning_type_dimensions check (
  (detectable_warning_type = 'None') =
    (detectable_warning_width = 0 and detectable_warning_length = 0)
);

create index sidx_curb_ramp_point_geom
on pedestrian.curb_ramp_point using gist(geom);

-- Curb ramp image table
create table pedestrian.curb_ramp_point_image (
    id serial primary key,
    related_id integer references pedestrian.curb_ramp_point(id)
        on delete cascade on update cascade,
    filename character varying(255)
);

-- Pedestrian signal table
create table pedestrian.pedestrian_signal_point (
    id serial primary key,
    signal_present boolean,
    button_count integer,
    button_height integer,
    button_size character varying(25),
    button_location character varying(25),
    button_surface boolean,
    button_spacing boolean,
    button_offset_curb boolean,
    button_contrast boolean,
    locator_tone boolean,
    tactile_arrow boolean,
    vibrotactile_signal boolean,
    passive_detection boolean,
    collection_date timestamp without time zone,
    region character varying(25),
    comment character varying(200),
    qa_status qa_status not null default 'Complete',
    qa_comment character varying(200),
    geom geometry(Point,3435) not null
);

alter table pedestrian.pedestrian_signal_point
add constraint check_required_fields check (
  case when qa_status in ('Not Started', 'Complete') then
    signal_present is distinct from null and
    button_location is distinct from null and
    button_size is distinct from null and
    button_contrast is distinct from null and
    tactile_arrow is distinct from null and
    vibrotactile_signal is distinct from null and 
    button_height is distinct from null and
    button_surface is distinct from null and
    button_spacing is distinct from null and
    button_offset_curb is distinct from null and
    button_count is distinct from null and
    locator_tone is distinct from null and
    passive_detection is distinct from null and
    collection_date is distinct from null
  else true end
);

alter table pedestrian.pedestrian_signal_point
add constraint check_button_location check (
  button_location in (
    'No Button',
    'Other',
    'Pedestrian Signal Pole',
    'Stub Pole',
    'Traffic Signal Pole'
  )
);

alter table pedestrian.pedestrian_signal_point
add constraint check_button_size check (
  button_size in (
    'Accessible',
    'Medium',
    'No Button',
    'Small'
  )
);

alter table pedestrian.pedestrian_signal_point
add constraint check_button_height check (
  button_height <= 60
);

alter table pedestrian.pedestrian_signal_point
add constraint check_button_count check (
  button_count >= 0 and button_count <= 2
);

alter table pedestrian.pedestrian_signal_point
add constraint check_button_fields check (
  (button_size = 'No Button') = (button_location = 'No Button') and
  (button_size = 'No Button') = (button_count = 0) and
  (button_size = 'No Button') = (button_height = 0)
);

create index sidx_pedestrian_signal_point_geom
on pedestrian.pedestrian_signal_point using gist(geom);

-- Pedestrian signal image table
create table pedestrian.pedestrian_signal_point_image (
    id serial primary key,
    related_id integer references pedestrian.pedestrian_signal_point(id)
        on delete cascade on update cascade,
    filename character varying(255)
);

-- Sidewalk point table
create table pedestrian.sidewalk_point (
    id serial primary key,
    point_type character varying(25) not null,
    material pedestrian.material,
    width integer,
    grade numeric(3,1),
    cross_slope numeric(3,1),
    buffer_type character varying(25),
    buffer_width integer,
    surface_condition pedestrian.surface_condition,
    vertical_fault_count integer,
    largest_vertical_fault pedestrian.largest_vertical_fault,
    cracked_panel_count integer,
    obstruction pedestrian.obstruction,
    collection_date timestamp without time zone,
    region character varying(25),
    comment character varying(200),
    qa_status qa_status not null default 'Complete',
    qa_comment character varying(200),
    geom geometry(Point,3435) not null
);

alter table pedestrian.sidewalk_point
add constraint check_required_fields check (
  case when qa_status in ('Not Started', 'Complete') then
    collection_date is distinct from null and
    case when (point_type = 'Summary') then
      material is distinct from null and
      cross_slope is distinct from null and
      surface_condition is distinct from null and
      vertical_fault_count is distinct from null and
      largest_vertical_fault is distinct from null and
      cracked_panel_count is distinct from null and
      obstruction is distinct from null and
      width is distinct from null
    when (point_type = 'Driveway') then
      cross_slope is distinct from null
    else true end
  else true end
);

alter table pedestrian.sidewalk_point
add constraint check_point_type check (
  point_type in (
    'Comment',
    'Driveway',
    'Local Issue',
    'Missing Sidewalk',
    'Summary'
  )
);

alter table pedestrian.sidewalk_point
add constraint check_slopes check (
  cross_slope <= 50.0
);

alter table pedestrian.sidewalk_point
add constraint check_width check (
  width <= 600
);

alter table pedestrian.sidewalk_point
add constraint check_buffer_type check (
  buffer_type in (
    'Landscaped',
    'None',
    'Solid',
    'Trees',
    'Vertical'
  )
);

alter table pedestrian.sidewalk_point
add constraint check_buffer check (
  (buffer_type = 'None') = (buffer_width = 0)
);

alter table pedestrian.sidewalk_point
add constraint check_vertical_faults check (
  (largest_vertical_fault = 'None') = (vertical_fault_count = 0)
);

create index sidx_sidewalk_point_geom
on pedestrian.sidewalk_point using gist(geom);

-- Sidewalk point image table
create table pedestrian.sidewalk_point_image (
    id serial primary key,
    related_id integer references pedestrian.sidewalk_point(id)
        on delete cascade on update cascade,
    filename character varying(255)
);

-- Sidewalk segment table
create table pedestrian.sidewalk_segment_geometry (
    id serial primary key,
    path_type character varying(25),
    safe_route_to_school boolean,
    municipality character varying(255),
    owner character varying(255),
    data_source character varying(255),
    region character varying(25),
    comment character varying(200),
    qa_status qa_status not null default 'Complete',
    qa_comment character varying(200),
    geom geometry(MultiLineString,3435) not null
);

create index sidx_sidewalk_segment_geometry_geom
on pedestrian.sidewalk_segment_geometry using gist(geom);
