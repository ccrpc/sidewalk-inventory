alter table sidewalk.crosswalk
add column marking_type character varying(25),
add column material pedestrian.material,
add column width integer,
add column cross_slope numeric(3,1),
add column stop_controlled_intersection boolean,
add column midblock_crossing boolean,
add column collection_date timestamp without time zone,
add column comment character varying(200),
add column image character varying(200);


alter table sidewalk.curb_ramp
add column ramp_type character varying(25),
add column edge_treatment character varying(25),
add column material pedestrian.material,
add column in_median boolean,
add column ramp_width integer,
add column ramp_length integer,
add column ramp_running_slope numeric(3,1),
add column ramp_cross_slope numeric(3,1),
add column detectable_warning_type character varying(25),
add column detectable_warning_width integer,
add column detectable_warning_length integer,
add column gutter_running_slope numeric(3,1),
add column gutter_cross_slope numeric(3,1),
add column landing_width integer,
add column landing_length integer,
add column landing_running_slope numeric(3,1),
add column landing_cross_slope numeric(3,1),
add column left_approach_width integer,
add column left_approach_running_slope numeric(3,1),
add column left_approach_cross_slope numeric(3,1),
add column right_approach_width integer,
add column right_approach_running_slope numeric(3,1),
add column right_approach_cross_slope numeric(3,1),
add column flare_slope numeric(3,1),
add column vertical_fault_count integer,
add column largest_vertical_fault pedestrian.largest_vertical_fault,
add column cracked_panel_count integer,
add column surface_condition pedestrian.surface_condition,
add column obstruction pedestrian.obstruction,
add column collection_date timestamp without time zone,
add column comment character varying(200),
add column image character varying(200);


alter table sidewalk.sidewalk
add column material pedestrian.material,
add column width integer,
add column grade numeric(3,1),
add column cross_slope numeric(3,1),
add column buffer_type character varying(25),
add column buffer_width integer,
add column surface_condition pedestrian.surface_condition,
add column vertical_fault_count integer,
add column largest_vertical_fault pedestrian.largest_vertical_fault,
add column cracked_panel_count integer,
add column obstruction pedestrian.obstruction,
add column collection_date timestamp without time zone,
add column comment character varying(200),
add column image character varying(200);


update sidewalk.crosswalk as cw
set marking_type = pt.marking_type,
material = pt.material,
width = pt.width,
cross_slope = pt.cross_slope,
stop_controlled_intersection = pt.stop_controlled_intersection,
midblock_crossing = pt.midblock_crossing,
collection_date = pt.collection_date,
comment = pt.comment,
image = pt.image,
region = pt.region,
qa_status = pt.qa_status,
qa_comment = pt.qa_comment
from pedestrian.crosswalk_point as pt
where cw.point_id = pt.id;


update sidewalk.curb_ramp as cr
set ramp_type = pt.ramp_type,
edge_treatment = pt.edge_treatment,
material = pt.material,
in_median = pt.in_median,
ramp_width = pt.ramp_width,
ramp_length = pt.ramp_length,
ramp_running_slope = pt.ramp_running_slope,
ramp_cross_slope = pt.ramp_cross_slope,
detectable_warning_type = pt.detectable_warning_type,
detectable_warning_width = pt.detectable_warning_width,
detectable_warning_length = pt.detectable_warning_length,
gutter_running_slope = pt.gutter_running_slope,
gutter_cross_slope = pt.gutter_cross_slope,
landing_width = pt.landing_width,
landing_length = pt.landing_length,
landing_running_slope = pt.landing_running_slope,
landing_cross_slope = pt.landing_cross_slope,
left_approach_width = pt.left_approach_width,
left_approach_running_slope = pt.left_approach_running_slope,
left_approach_cross_slope = pt.left_approach_cross_slope,
right_approach_width = pt.right_approach_width,
right_approach_running_slope = pt.right_approach_running_slope,
right_approach_cross_slope = pt.right_approach_cross_slope,
flare_slope = pt.flare_slope,
vertical_fault_count = pt.vertical_fault_count,
largest_vertical_fault = pt.largest_vertical_fault,
cracked_panel_count = pt.cracked_panel_count,
surface_condition = pt.surface_condition,
obstruction = pt.obstruction,
collection_date = pt.collection_date,
comment = pt.comment,
image = pt.image,
region = pt.region,
qa_status = pt.qa_status,
qa_comment = pt.qa_comment
from pedestrian.curb_ramp_point as pt
where cr.point_id = pt.id;


update sidewalk.sidewalk as sw
set material = pt.material,
width = pt.width,
grade = pt.grade,
cross_slope = pt.cross_slope,
buffer_type = pt.buffer_type,
buffer_width = pt.buffer_width,
surface_condition = pt.surface_condition,
vertical_fault_count = pt.vertical_fault_count,
largest_vertical_fault = pt.largest_vertical_fault,
cracked_panel_count = pt.cracked_panel_count,
obstruction = pt.obstruction,
collection_date = pt.collection_date,
comment = pt.comment,
image = pt.image,
region = pt.region,
qa_status = pt.qa_status,
qa_comment = pt.qa_comment
from pedestrian.sidewalk_point as pt
where sw.point_id = pt.id
  and pt.point_type = 'Summary';