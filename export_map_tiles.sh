#!/bin/bash

if [[ -z "${PG_CONNECTION_STRING}" ]]; then
  echo "Environment variable PG_CONNECTION_STRING is required and must contain the database connection string."
  exit 1
fi

# Set variable with layer configuration.
read -r -d '' layer_config << EOM
{
  "pedestrian.sidewalk_network_score_hexagon": {
    "target_name": "aggregated",
    "description": "Sidewalk network ADA compliance and condition scores aggregated to half-mile hexagons",
    "minzoom": 0,
    "maxzoom": 12
  },
  "pedestrian.sidewalk_score": {
    "target_name": "sidewalk",
    "description": "Sidewalk ADA compliance and condition scores",
    "minzoom": 13,
    "maxzoom": 14
  },
  "pedestrian.curb_ramp_score": {
    "target_name": "curb_ramp",
    "description": "Curb ramp ADA compliance and condition scores",
    "minzoom": 13,
    "maxzoom": 14
  },
  "pedestrian.crosswalk_score": {
    "target_name": "crosswalk",
    "description": "Crosswalk ADA compliance and condition scores",
    "minzoom": 13,
    "maxzoom": 14
  },
  "pedestrian.pedestrian_signal_score": {
    "target_name": "pedestrian_signal",
    "description": "Pedestrian signal ADA compliance and condition scores",
    "minzoom": 13,
    "maxzoom": 14
  }
}
EOM

# Prompt the user for the year.
echo "Please enter the year being exported:"
read year

echo "$year.mbtiles"

# Convert the layers to map tiles.
ogr2ogr -f MBTILES "$year.mbtiles" \
  PG:"$PG_CONNECTION_STRING" \
  -oo TABLES='pedestrian.sidewalk_network_score_hexagon,pedestrian.sidewalk_score,pedestrian.curb_ramp_score,pedestrian.crosswalk_score,pedestrian.pedestrian_signal_score' \
  -dsco NAME="$year Sidewalk Network Inventory and Assessment" \
  -dsco DESCRIPTION="$year compliance and condition scores from the Sidewalk Network Inventory and Assessment" \
  -dsco MINZOOM=0 \
  -dsco MAXZOOM=14 \
  -dsco CONF="$layer_config"
