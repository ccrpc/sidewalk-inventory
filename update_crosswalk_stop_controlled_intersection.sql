with crosswalk_leg as (
	select crosswalk_id,
		intersection_id,
		case when direction > 45 and direction <= 135 then 'E'
			when direction > 135 and direction <= 225 then 'S'
			when direction > 225 and direction <= 315 then 'W'
			else 'N' end as leg
	from (
		select distinct on (cw.id)
			cw.id as crosswalk_id,
			i.id as intersection_id,
			degrees(st_azimuth(i.geom, cw.geom)) as direction
		from street.intersection as i
		inner join pedestrian.crosswalk_point as cw
			on st_dwithin(i.geom, cw.geom, 100)
		order by cw.id,
			st_distance(i.geom, cw.geom)
	) as crosswalk_intersection
), intersection_stop as (
	select id as intersection_id,
		control_type,
		control_type = 'AWSC' or control_type like '%SC (%S%)' as stop_north_leg,
		control_type = 'AWSC' or control_type like '%SC (%W%)' as stop_east_leg,
		control_type = 'AWSC' or control_type like '%SC (%N%)' as stop_south_leg,
		control_type = 'AWSC' or control_type like '%SC (%E%)' as stop_west_leg
	from street.intersection
)
update pedestrian.crosswalk_point
set stop_controlled_intersection = true
from (
	select cw.id
	from crosswalk_leg as leg
	inner join pedestrian.crosswalk_point as cw
		on cw.id = leg.crosswalk_id
	inner join intersection_stop as stop
		on leg.intersection_id = stop.intersection_id
	where cw.stop_controlled_intersection = false and (
		(leg.leg = 'N' and stop.stop_north_leg) or
		(leg.leg = 'E' and stop.stop_east_leg) or
		(leg.leg = 'S' and stop.stop_south_leg) or
		(leg.leg = 'W' and stop.stop_west_leg)
	)
) as cw_stop
where pedestrian.crosswalk_point.id = cw_stop.id;
