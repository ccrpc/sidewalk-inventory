# Sidewalk Network Inventory and Assessment

This is the SQL-based data model for the Sidewalk Network
Inventory and Assessment for the Champaign Urbana Urbanized Area. For the
previous Python-based data model, see [the `python-datamodel` branch][1].

## Credits

Sidewalk Network Inventory and Assessment was originally developed by Matt Yoder and
Wenhao Gu for the [Champaign County Regional Planning Commission][2].

## License
Sidewalk Network Inventory and Assessment is available under the terms of the
[BSD 3-clause license][3].

[1]: https://gitlab.com/ccrpc/sidewalk-inventory/tree/python-datamodel
[2]: https://ccrpc.org
[3]: https://gitlab.com/ccrpc/sidewalk-inventory/blob/master/LICENSE.md
